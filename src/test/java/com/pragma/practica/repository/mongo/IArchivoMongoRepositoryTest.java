package com.pragma.practica.repository.mongo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.pragma.practica.entity.Archivo;

@ExtendWith(MockitoExtension.class)
class IArchivoMongoRepositoryTest {

	@Mock
	private IArchivoMongoRepository mongoRepository;

	Archivo archivo1;

	@BeforeEach
	void setUp() {
		this.archivo1 = new Archivo(1L, "PruebaMongo", 1L);
	}

	@Test
	void findByIdUsuario() {
		Mockito.when(mongoRepository.findByIdUsuario(1L)).thenReturn(this.archivo1);
		Archivo respuesta = mongoRepository.findByIdUsuario(1L);

		assertNotNull(respuesta);
		assertEquals("PruebaMongo", respuesta.getInformacion());
	}
}