package com.pragma.practica.repository.mongo;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.pragma.practica.entity.Archivo;

@ExtendWith(MockitoExtension.class)
class PersonalizadaArchivoRepositoryImpTest {

	@Mock
	private PersonalizadaArchivoRepositoryImp personalizadaArchivoRepositoryImp;

	Archivo archivo1;

	@BeforeEach
	void setUp() {
		this.archivo1 = new Archivo(1L, "PruebaMongo", 1L);
	}

	@Test
	void updateArchivoInformacion() {

		Mockito.when(personalizadaArchivoRepositoryImp.updateArchivoInformacion(1L, "Prueba actualizacion información"))
				.thenReturn(true);
		boolean respuesta = personalizadaArchivoRepositoryImp.updateArchivoInformacion(1L,
				"Prueba actualizacion información");

		assertNotNull(respuesta);
		assertTrue(respuesta);
	}
}