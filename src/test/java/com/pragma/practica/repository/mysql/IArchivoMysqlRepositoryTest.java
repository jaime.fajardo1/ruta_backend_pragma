package com.pragma.practica.repository.mysql;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.pragma.practica.entity.Archivo;

@ExtendWith(MockitoExtension.class)
class IArchivoMysqlRepositoryTest {

	Archivo archivo1;

	@Mock
	private IArchivoMysqlRepository archivoMysqlRepository;

	@BeforeEach
	void setUp() {
		this.archivo1 = new Archivo(1L, "Prueba", 1L);
	}

	@Test
	void findByIdUsuario() {
		Mockito.when(archivoMysqlRepository.findByIdUsuario(1L)).thenReturn(this.archivo1);
		Archivo respuesta = archivoMysqlRepository.findByIdUsuario(1L);

		assertNotNull(respuesta);
		assertEquals("Prueba", respuesta.getInformacion());
	}
}