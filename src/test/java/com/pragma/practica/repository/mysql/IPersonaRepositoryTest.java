package com.pragma.practica.repository.mysql;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.pragma.practica.entity.Archivo;
import com.pragma.practica.entity.Persona;

@ExtendWith(MockitoExtension.class)
class IPersonaRepositoryTest {

	@Mock
	private IPersonaRepository personaRepository;

	Persona persona1;
	Persona persona2;
	Archivo archivo1;
	Archivo archivo2;

	@BeforeEach
	void setUp() {

		this.archivo1 = new Archivo(1L, "Prueba", 1L);
		this.archivo2 = new Archivo(2L, "Prueba2", 2L);

		this.persona1 = new Persona(1L, "Jaime", "Fajardo", "CC", "1144203456", 24, "Cali", archivo1,
				archivo1.getInformacion());
		this.persona2 = new Persona(2L, "Jose", "Perez", "CC", "1144203789", 25, "Medellín", archivo2,
				archivo2.getInformacion());
	}

	@Test
	void findByEdadGreaterThan() {
		Mockito.when(personaRepository.findByEdadGreaterThanEqual(24)).thenReturn(Arrays.asList(this.persona2));
		List<Persona> respuesta = personaRepository.findByEdadGreaterThanEqual(24);

		assertEquals(1, respuesta.size());
		assertEquals("Jose", respuesta.get(0).getNombres());
	}

	@Test
	void findByEdadGreaterThanEqual() {

		Mockito.when(personaRepository.findByEdadGreaterThanEqual(23))
				.thenReturn(Arrays.asList(this.persona1, this.persona2));
		List<Persona> respuesta = personaRepository.findByEdadGreaterThanEqual(23);

		assertEquals(2, respuesta.size());
		assertEquals("Jaime", respuesta.get(0).getNombres());

	}
}