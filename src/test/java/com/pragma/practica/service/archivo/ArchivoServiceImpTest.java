package com.pragma.practica.service.archivo;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;

import com.pragma.practica.entity.Archivo;
import com.pragma.practica.repository.mongo.IArchivoMongoRepository;
import com.pragma.practica.repository.mongo.IPersonalizadaArchivoRepository;
import com.pragma.practica.repository.mysql.IArchivoMysqlRepository;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class ArchivoServiceImpTest {

	@Mock
	IArchivoMysqlRepository repositorioMysqlArchivo;

	@Mock
	IArchivoMongoRepository repositorioMongoArchivo;

	@Mock
	ArchivoService archivoService = new ArchivoServiceImp();

	@Mock
	IPersonalizadaArchivoRepository repositorioArchivoPersonalizado;

	@InjectMocks
	ArchivoServiceImp archivoServiceImp = new ArchivoServiceImp("http://localhost:9090/api/archivos/img/Perfil_",
			"imagenes/", "Perfil_");

	@Value("${app.rutaConsultaArchivos}")
	private String rutaConsultaArchivos;

	Archivo archivo1;
	Archivo archivo2;

	@BeforeEach
	void setUp() {

		archivo1 = new Archivo(1L, "http://localhost:9090/api/archivos/img/Perfil_1.jpg", 1L);
		archivo2 = new Archivo(2L, "http://localhost:9090/api/archivos/img/Perfil_1.jpg", 2L);

		Mockito.when(repositorioMysqlArchivo.save(archivo1)).thenReturn(archivo1);
		Mockito.when(repositorioMongoArchivo.save(archivo1)).thenReturn(archivo1);
		Mockito.when(repositorioMongoArchivo.findByIdUsuario(1L)).thenReturn(archivo1);
	}

	@Test
	void guardarArchivoBD() throws IOException {

		Path root = Paths.get("imagenes/");
		Path path = root.resolve("Perfil_1.jpg");
		File file = new File(path.toString());

		MockMultipartFile archivoImg = new MockMultipartFile("imagen", file.getName(), MediaType.IMAGE_JPEG_VALUE,
				new FileInputStream(file));

		boolean respuesta = archivoServiceImp.guardarArchivoBD(archivoImg, 1L);

		assertNotNull(respuesta);
		assertTrue(respuesta);
	}

	@Test
	void actualizarArchivoBD() throws IOException {
		Path root = Paths.get("imagenes/");
		Path path = root.resolve("Perfil_3.jpg");
		File file = new File(path.toString());

		MockMultipartFile archivoImg = new MockMultipartFile("imagen", file.getName(), MediaType.IMAGE_JPEG_VALUE,
				new FileInputStream(file));

		boolean respuesta = archivoServiceImp.actualizarArchivoBD(archivoImg, 1L);

		assertNotNull(respuesta);
		assertTrue(respuesta);
	}

	@Test
	void borrarBD() {
		boolean respuesta = archivoServiceImp.borrarBD(1L);

		assertNotNull(respuesta);
		assertTrue(respuesta);
	}
}