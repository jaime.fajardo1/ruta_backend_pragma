package com.pragma.practica.service.persona;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import com.pragma.practica.entity.Archivo;
import com.pragma.practica.entity.Persona;
import com.pragma.practica.repository.mongo.IArchivoMongoRepository;
import com.pragma.practica.repository.mysql.IArchivoMysqlRepository;
import com.pragma.practica.repository.mysql.IPersonaRepository;
import com.pragma.practica.service.archivo.ArchivoService;
import com.pragma.practica.service.archivo.ArchivoServiceImp;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class PersonaServiceImpTest {

	@Mock
	IPersonaRepository repositorioPersona;

	@Mock
	IArchivoMysqlRepository repositorioMysqlArchivo;

	@Mock
	IArchivoMongoRepository repositorioMongoArchivo;

	@Mock
	ArchivoService archivoService = new ArchivoServiceImp();

	@InjectMocks
	PersonaServiceImp personaServiceImp;

	Persona persona1;
	Persona persona2;
	Archivo archivo1;
	Archivo archivo2;

	@BeforeEach
	void setUp() {

		archivo1 = new Archivo(1L, "Prueba", 1L);
		archivo2 = new Archivo(2L, "Prueba2", 2L);

		persona1 = new Persona(1L, "Jaime", "Fajardo", "CC", "1144203456", 24, "Cali", archivo1,
				archivo1.getInformacion());
		persona2 = new Persona(2L, "Jose", "Perez", "CC", "1144203789", 25, "Medellín", archivo2,
				archivo2.getInformacion());

		/* Traer Todo */
		Mockito.when(repositorioMysqlArchivo.findAll()).thenReturn(Arrays.asList(archivo1, archivo2));
		Mockito.when(repositorioMongoArchivo.findAll()).thenReturn(Arrays.asList(archivo1, archivo2));
		Mockito.when(repositorioPersona.findAll()).thenReturn(Arrays.asList(persona1, persona2));

	}

	@Test
	void listar() {
		List<Persona> lstRespuesta = personaServiceImp.listar();
		assertNotNull(lstRespuesta);
		assertEquals("Jaime", lstRespuesta.get(0).getNombres());
	}

	@Test
	void listarMayorIgual() {
		int edadPrueba = 23;
		boolean mayorIgual = true;
		/* Traer Edad */
		Mockito.when(repositorioPersona.findByEdadGreaterThan(edadPrueba)).thenReturn(Arrays.asList(persona2));
		Mockito.when(repositorioPersona.findByEdadGreaterThanEqual(edadPrueba))
				.thenReturn(Arrays.asList(persona1, persona2));

		List<Persona> lstRespuesta = personaServiceImp.listarMayorIgual(edadPrueba, mayorIgual);

		assertNotNull(lstRespuesta);
		assertEquals("Jaime", lstRespuesta.get(0).getNombres());
		assertEquals("Perez", lstRespuesta.get(1).getApellidos());
		assertEquals("CC", lstRespuesta.get(0).getTipoIdentificacion());
	}

	@Test
	void crear() {
		Mockito.when(repositorioPersona.save(persona2)).thenReturn(persona2);
		Persona respuesta = personaServiceImp.crear(persona2);
		assertNotNull(respuesta);
		assertEquals("Medellín", respuesta.getCiudad());
		assertEquals("1144203789", respuesta.getNumeroIdentificacion());
		assertEquals(25, respuesta.getEdad());
	}

	@Test
	void actualizar() {
		Persona personaActualizada = new Persona();
		personaActualizada.setNombres("Camilo");
		personaActualizada.setPerfil(archivo1);
		personaActualizada.setPerfil64(archivo1.getInformacion());

		Mockito.when(repositorioPersona.save(persona1)).thenReturn(personaActualizada);
		Persona respuesta = personaServiceImp.actualizar(persona1);
		assertNotNull(respuesta);
		assertEquals("Camilo", respuesta.getNombres());
		assertEquals(archivo1, respuesta.getPerfil());
		assertEquals("Prueba", respuesta.getPerfil64());
	}

	@Test
	void eliminar() {
		personaServiceImp.eliminar(1L);
		Mockito.verify(repositorioPersona).deleteById(1L);
	}
}