package com.pragma.practica.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.pragma.practica.service.archivo.ArchivoService;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/archivos")
public class ArchivoController {

	@Autowired
	private ArchivoService archivoService;
	

	@PostMapping
	public ResponseEntity<?> crear(@Valid @RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long idUsuario) {
		archivoService.guardarArchivoBD(archivo, idUsuario);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@GetMapping("/img/{nombreArchivo:.+}")
	public ResponseEntity<?> listarArchivoId(@PathVariable String nombreArchivo) {

		Resource file = archivoService.cargarArchivo(nombreArchivo);

		return ResponseEntity.ok()
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
				.body(file);
	}

	@PutMapping
	public ResponseEntity<?> actualizarArchivo(@Valid @RequestParam("archivo") MultipartFile archivo, @RequestParam("id") Long idUsuario) {
		archivoService.actualizarArchivoBD(archivo, idUsuario);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}
	
	@DeleteMapping
	public ResponseEntity<?>  eliminarArchivo(@RequestParam("id") Long idUsuario) {
		archivoService.borrarBD(idUsuario);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
