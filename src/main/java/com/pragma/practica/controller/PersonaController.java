package com.pragma.practica.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pragma.practica.entity.Persona;
import com.pragma.practica.service.persona.IPersonaService;

import io.swagger.v3.oas.annotations.Operation;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/personas")
public class PersonaController {

	@Autowired
	private IPersonaService personaService;
	
	Map<String,Object> response = new HashMap<>();

	@Operation(summary = "Consulta todas las personas")
	@GetMapping
	public ResponseEntity<?> listar() {
		return new ResponseEntity<List<Persona>>(personaService.listar(),HttpStatus.OK);
	}

	@Operation(summary = "Consulta todas las personas de acuerdo a los parametros edad y mayorIgual")
	@GetMapping(path = "/filtro")
	public ResponseEntity<?> listarMayorIgual(@RequestParam int edad,
			@RequestParam(defaultValue = "false", required = false) boolean mayorIgual) {
		return new ResponseEntity<List<Persona>>(personaService.listarMayorIgual(edad, mayorIgual),HttpStatus.OK);
	}

	@PostMapping
	public ResponseEntity<?> crear(@Valid @RequestBody Persona persona) {
		response.put("Persona",personaService.crear(persona));
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
		
	}

	@PutMapping
	public ResponseEntity<?> actualizar(@Valid @RequestBody Persona persona) {
		response.put("Persona", personaService.actualizar(persona));
		return new ResponseEntity<Map<String,Object>>(response,HttpStatus.CREATED);
	}

	@DeleteMapping(value = "/{id}")
	public ResponseEntity<?> eliminar(@PathVariable("id") Long id) {
		personaService.eliminar(id);
		return new ResponseEntity<>(HttpStatus.OK);
	}
	
}
