package com.pragma.practica;

import javax.annotation.Resource;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.pragma.practica.service.archivo.ArchivoService;

@SpringBootApplication
@EnableJpaRepositories (basePackages = {"com.pragma.practica.repository.mysql"})
@EnableMongoRepositories(basePackages={"com.pragma.practica.repository.mongo"})
public class PracticaApplication implements CommandLineRunner{
	
	@Resource
	ArchivoService archivoService;

	public static void main(String[] args) {
		SpringApplication.run(PracticaApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception {
		archivoService.init();
	}

}
