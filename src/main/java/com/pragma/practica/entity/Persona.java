package com.pragma.practica.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="personas")
@Data
@NoArgsConstructor
public class Persona {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "El nombre es requerido")
	private String nombres;
	
	@NotEmpty(message = "El apellido es requerido")
	private String apellidos;
	
	@NotEmpty(message = "Tipo de identificacion es requerido")
	private String tipoIdentificacion;
	
	@Column(unique=true)
	@NotEmpty(message = "Número de identificacion es requerido")
	private String numeroIdentificacion;
	
	private int edad;
	private String ciudad;
	@Transient
	private Archivo perfil;
	
	@Transient
	private String perfil64;

	public Persona(Long id, String nombres, String apellidos, String tipoIdentificacion, String numeroIdentificacion, int edad, String ciudad, Archivo perfil, String perfil64) {
		this.id = id;
		this.nombres = nombres;
		this.apellidos = apellidos;
		this.tipoIdentificacion = tipoIdentificacion;
		this.numeroIdentificacion = numeroIdentificacion;
		this.edad = edad;
		this.ciudad = ciudad;
		this.perfil = perfil;
		this.perfil64 = perfil64;
	}

}
