package com.pragma.practica.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "archivos")
@Document(collection="archivos")
@Data
@NoArgsConstructor
public class Archivo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NotEmpty(message = "Información es requerido")
	private String informacion;
	
	@NotNull(message = "Id usuario es requerido")
	private Long idUsuario;

	public Archivo(Long id, String informacion, Long idUsuario) {
		this.id = id;
		this.informacion = informacion;
		this.idUsuario = idUsuario;
	}
}
