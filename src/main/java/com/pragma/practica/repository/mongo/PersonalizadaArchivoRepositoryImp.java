package com.pragma.practica.repository.mongo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.stereotype.Component;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.data.mongodb.core.query.Criteria;

import com.mongodb.client.result.UpdateResult;
import com.pragma.practica.entity.Archivo;

@Component
public class PersonalizadaArchivoRepositoryImp implements IPersonalizadaArchivoRepository{

    @Autowired
    MongoTemplate mongoTemplate;
	
	@Override
	public boolean updateArchivoInformacion(Long idUsuario, String nuevaInformacion) {
		Query query = new Query(Criteria.where("idUsuario").is(idUsuario));
        Update update = new Update();
        update.set("informacion", nuevaInformacion);
        
        UpdateResult result = mongoTemplate.updateFirst(query, update, Archivo.class);
		return true;
	}

}
