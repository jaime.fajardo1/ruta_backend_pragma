package com.pragma.practica.repository.mongo;

public interface IPersonalizadaArchivoRepository {

	boolean updateArchivoInformacion(Long idUsuario,String nuevaInformacion);
}
