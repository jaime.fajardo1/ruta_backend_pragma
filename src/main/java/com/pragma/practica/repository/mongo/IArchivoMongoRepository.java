package com.pragma.practica.repository.mongo;

import javax.transaction.Transactional;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.pragma.practica.entity.Archivo;


@Transactional
public interface IArchivoMongoRepository extends MongoRepository<Archivo, Long> {

	Archivo findByIdUsuario(Long idUsuario);
}
