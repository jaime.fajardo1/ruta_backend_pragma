package com.pragma.practica.repository.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.practica.entity.Persona;

public interface IPersonaRepository extends JpaRepository<Persona, Long> {

	//mayor
	List<Persona> findByEdadGreaterThan(int edad);
	
	//mayor o igual
	List<Persona> findByEdadGreaterThanEqual(int edad);
}
