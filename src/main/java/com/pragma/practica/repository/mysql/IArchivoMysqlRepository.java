package com.pragma.practica.repository.mysql;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.pragma.practica.entity.Archivo;


@Transactional
public interface IArchivoMysqlRepository extends JpaRepository<Archivo, Long>{

	Archivo findByIdUsuario(Long idUsuario);
	
}
