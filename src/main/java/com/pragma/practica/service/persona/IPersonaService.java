package com.pragma.practica.service.persona;

import java.util.List;

import com.pragma.practica.entity.Persona;

public interface IPersonaService {

	List<Persona> listar();
	
	List<Persona> listarMayorIgual(int edad, boolean mayorIgual);
	
	Persona crear(Persona persona);
	
	Persona actualizar(Persona persona);

	boolean eliminar(Long id);
}
