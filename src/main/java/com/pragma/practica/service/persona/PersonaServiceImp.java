package com.pragma.practica.service.persona;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pragma.practica.entity.Archivo;
import com.pragma.practica.entity.Persona;
import com.pragma.practica.repository.mongo.IArchivoMongoRepository;
import com.pragma.practica.repository.mysql.IArchivoMysqlRepository;
import com.pragma.practica.repository.mysql.IPersonaRepository;
import com.pragma.practica.service.archivo.ArchivoService;

@Service
public class PersonaServiceImp implements IPersonaService {

	@Autowired
	private IPersonaRepository repositorioPersona;

	@Autowired
	private IArchivoMysqlRepository repositorioMysqlArchivo;

	@Autowired
	private IArchivoMongoRepository repositorioMongoArchivo;

	@Autowired
	private ArchivoService archivoService;

	@Override
	public List<Persona> listar() {
		
		List<Archivo> lstArchivos = repositorioMysqlArchivo.findAll();
		List<Archivo> lstArchivosMongo = repositorioMongoArchivo.findAll();
		
		List<Persona> lstPersonas = repositorioPersona.findAll().stream().map(p -> {

			
			if(lstArchivos.stream().filter(a -> a.getIdUsuario().equals(p.getId())).findFirst().isPresent()) {
				p.setPerfil(lstArchivos.stream().filter(a -> a.getIdUsuario().equals(p.getId())).findFirst().get());
				p.setPerfil64(lstArchivosMongo.stream().filter(a -> a.getIdUsuario().equals(p.getId())).findFirst().get().getInformacion());
			}else {
				p.setPerfil(null);
			}

			return p;
		}).collect(Collectors.toList());

		return lstPersonas;
	}

	@Override
	public List<Persona> listarMayorIgual(int edad, boolean mayorIgual) {
		List<Archivo> lstArchivos = repositorioMysqlArchivo.findAll();
		List<Archivo> lstArchivosMongo = repositorioMongoArchivo.findAll();
		
		List<Persona> lstPersonas;

		if (mayorIgual) {
			lstPersonas = repositorioPersona.findByEdadGreaterThanEqual(edad);
		} else {
			lstPersonas = repositorioPersona.findByEdadGreaterThan(edad);
		}

		lstPersonas.stream().map(p -> {

			if(lstArchivos.stream().filter(a -> a.getIdUsuario().equals(p.getId())).findFirst().isPresent()) {
				p.setPerfil(lstArchivos.stream().filter(a -> a.getIdUsuario().equals(p.getId())).findFirst().get());
				p.setPerfil64(lstArchivosMongo.stream().filter(a -> a.getIdUsuario().equals(p.getId())).findFirst().get().getInformacion());
			}else {
				p.setPerfil(null);
			}

			return p;
		}).collect(Collectors.toList());

		return lstPersonas;
	}

	@Override
	public Persona crear(Persona persona) {
		return repositorioPersona.save(persona);
	}

	@Override
	public Persona actualizar(Persona persona) {
		return repositorioPersona.save(persona);
	}

	@Override
	public boolean eliminar(Long id) {
		archivoService.borrarBD(id);
		repositorioPersona.deleteById(id);
		return true;
	}

}
