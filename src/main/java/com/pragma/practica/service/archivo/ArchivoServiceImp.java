package com.pragma.practica.service.archivo;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.pragma.practica.entity.Archivo;
import com.pragma.practica.repository.mongo.IArchivoMongoRepository;
import com.pragma.practica.repository.mongo.IPersonalizadaArchivoRepository;
import com.pragma.practica.repository.mysql.IArchivoMysqlRepository;

@Service
public class ArchivoServiceImp implements ArchivoService {


	private String rutaConsultaArchivos;
	private String carpetaArchivos;
	private String prefijoImagen;

	@Autowired
	private ArchivoService archivoService;

	@Autowired
	private IArchivoMysqlRepository repositorioMysqlArchivo;

	@Autowired
	private IArchivoMongoRepository repositorioMongoArchivo;

	@Autowired
	IPersonalizadaArchivoRepository repositorioArchivoPersonalizado;

	public ArchivoServiceImp(){}
	
	@Autowired
	public ArchivoServiceImp(@Value("${app.rutaConsultaArchivos}") String rutaConsultaArchivos,@Value("${app.carpetaArchivos}") String carpetaArchivos,@Value("${app.prefijoImagen}") String prefijoImagen){
		this.rutaConsultaArchivos = rutaConsultaArchivos;
		this.carpetaArchivos = carpetaArchivos;
		this.prefijoImagen = prefijoImagen;
	}

	@Override
	public void init() {
		
		Path root = Paths.get(this.carpetaArchivos);
		try {
			if (!Files.exists(root)) {
				Files.createDirectory(root);
			}

		} catch (IOException e) {
			throw new RuntimeException("No se puede inicializar la carpeta imagenes");
		}
	}

	/* Guardado de la imagen en la carpeta */
	@Override
	public void guardarArchivo(MultipartFile file, Long idUsuario) {
		try {
			Path root = Paths.get(this.carpetaArchivos);
			String extension = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(".") + 1);

			if (Files.exists(root.resolve(this.prefijoImagen + idUsuario + "." + extension))) {
				Files.delete(root.resolve(this.prefijoImagen + idUsuario + "." + extension));
			}

			Files.copy(file.getInputStream(), root.resolve(this.prefijoImagen + idUsuario + "." + extension));

		} catch (IOException e) {
			throw new RuntimeException("No se puede guardar el archivo. Error " + e.getMessage());
		}

	}

	@Override
	public Resource cargarArchivo(String nombreArchivo) {
		try {
			Path root = Paths.get(this.carpetaArchivos);
			Path file = root.resolve(nombreArchivo);
			Resource resource = new UrlResource(file.toUri());

			if (resource.exists() || resource.isReadable()) {
				return resource;
			} else {
				throw new RuntimeException("No se puede leer el archivo ");
			}

		} catch (MalformedURLException e) {
			throw new RuntimeException("Error: " + e.getMessage());
		}
	}

	@Override
	public String borrarArchivo(String nombre) {
		Path root = Paths.get(this.carpetaArchivos);
		try {
			Files.deleteIfExists(root.resolve(nombre));
			return "Borrado";
		} catch (IOException e) {
			e.printStackTrace();
			return "Error Borrando ";
		}
	}

	@Override
	public boolean guardarArchivoBD(MultipartFile archivo, Long idUsuario) {


		/* Guarda en carpeta la imagen */
		archivoService.guardarArchivo(archivo, idUsuario);

		String extension = archivo.getOriginalFilename().substring(archivo.getOriginalFilename().lastIndexOf(".") + 1);
		String rutaCompleta = this.rutaConsultaArchivos + idUsuario + "." + extension;

		Archivo objArchivo = new Archivo();
		
		objArchivo.setIdUsuario(idUsuario);
		objArchivo.setInformacion(rutaCompleta);

		/* Guarda la informacion en Mysql */
		repositorioMysqlArchivo.save(objArchivo);

		/* Guarda la informacion en Mongo */
		try {
			String rutaAlmacenamiento = Paths.get(this.carpetaArchivos + this.prefijoImagen + idUsuario + "." + extension)
					.toString();
			byte[] fileContent = FileUtils.readFileToByteArray(new File(rutaAlmacenamiento));
			String imagenBase64 = Base64.getEncoder().encodeToString(fileContent);

			objArchivo.setInformacion(imagenBase64);
			repositorioMongoArchivo.save(objArchivo);

		} catch (IOException e) {
			System.out.println("error en Archivo - guardarBD");
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public boolean actualizarArchivoBD(MultipartFile archivo, Long idUsuario) {

		/* Guarda en carpeta la imagen */
		archivoService.guardarArchivo(archivo, idUsuario);

		String extension = archivo.getOriginalFilename().substring(archivo.getOriginalFilename().lastIndexOf(".") + 1);
		String rutaCompleta = this.rutaConsultaArchivos + idUsuario + "." + extension;

		Archivo objArchivo = new Archivo();
		objArchivo.setIdUsuario(idUsuario);
		objArchivo.setInformacion(rutaCompleta);
		
		/*si no existe en mysql lo creamos*/
		if(repositorioMysqlArchivo.findByIdUsuario(idUsuario) == null) {
			repositorioMysqlArchivo.save(objArchivo);
		}
		
		/*Mongo*/
		String imagenBase64 = "";
		
		try {
			String rutaAlmacenamiento = Paths.get(this.carpetaArchivos + this.prefijoImagen + idUsuario + "." + extension)
					.toString();
			byte[] fileContent = FileUtils.readFileToByteArray(new File(rutaAlmacenamiento));
			imagenBase64 = Base64.getEncoder().encodeToString(fileContent);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(repositorioMongoArchivo.findByIdUsuario(idUsuario) == null) {
			/*No existe en mongo hay que crearlo*/
			objArchivo.setInformacion(imagenBase64);
			repositorioMongoArchivo.save(objArchivo);
		}else {
			/*ya existe solo se actualiza*/
			repositorioArchivoPersonalizado.updateArchivoInformacion(idUsuario, imagenBase64);
		}

		return true;
	}

	@Override
	public boolean borrarBD(Long idUsuario) {
		Archivo objArchivo = repositorioMysqlArchivo.findByIdUsuario(idUsuario);

		if (objArchivo != null) {
			String informacion = objArchivo.getInformacion();
			String nombre = informacion.substring(informacion.lastIndexOf("/") + 1);

			archivoService.borrarArchivo(nombre);
			repositorioMysqlArchivo.deleteById(objArchivo.getId());
			repositorioMongoArchivo.deleteById(objArchivo.getId());
		}

		return true;
	}

}
