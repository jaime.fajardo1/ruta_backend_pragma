package com.pragma.practica.service.archivo;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface ArchivoService {
	
	public void init();
	
	public void guardarArchivo(MultipartFile file,Long idUsuario);
	
	public String borrarArchivo(String nombre);
	
	public Resource cargarArchivo(String nombreArchivo);
	
	public boolean guardarArchivoBD(MultipartFile archivo, Long idUsuario);
	
	public boolean actualizarArchivoBD(MultipartFile archivo, Long idUsuario);
	
	public boolean borrarBD(Long idUsuario);
}
